# Deprecated Repository

**Warning:** This repository is deprecated. It is only hosted for historical purposes. Please switch to [the new repository](https://git.noahvogt.com/noah/xdg-repo).